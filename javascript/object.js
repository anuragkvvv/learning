// 1 = show two name
var names = {
  name1: "anuj",
  name2: "varun",
  name3: "rahul",
  name4: "ankit"
};
document.write(names.name3 + "  " + names.name1);

// 2 =function inside object
var biodata = {
  name: "vijay",
  serName: "singh",
  class: 8,
  rollNo: 36,
  height: "5feet",
  weight: "40kg",
  body: function() {
    return this.weight + this.height;
  }
};
document.write("   name" + biodata.name + "   body structure" + biodata.body());

//3 = array inside object
var number = {
  evenNumber: [2, 4, 6, 8, 10],
  oddNumber: [1, 3, 5, 7, 9]
};
document.write("   even" + number.evenNumber);
document.write("   odd" + number.oddNumber);

//4=
